package com.shruthitech.xmltoexcel;

import java.io.File;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

public class App {
	public static void main(String[] args) throws IOException, ValidityException, ParsingException {
		App app = new App();
		Workbook book = app.getWorkbook();
		Document doc = app.readXML("src/main/resources/media.xml");
		app.buildExcel(doc, book);
	}

	private Workbook getWorkbook() throws IOException {
		Workbook workbook = null;
		workbook = new XSSFWorkbook();
		return workbook;
	}
	
	private Document readXML(final String src) throws ValidityException, ParsingException, IOException{
		Builder builder = new Builder();
		Document doc = builder.build(new File(src));
		return doc;
	}
	
	private void buildExcel(Document doc, Workbook book){
		Element ele = doc.getRootElement();
		printNodes(ele, "");
	
	}
	
	private void printNodes(Element ele, String depth){
		System.out.println(depth + ele.getLocalName());
		int numAttr = ele.getAttributeCount();
		for(int i=0; i < numAttr; i++){
			System.out.println(depth + "\t'@" + ele.getAttribute(i).getLocalName());
		}
		Elements eles = ele.getChildElements();
		if(eles.size() > 0){
			depth = depth + "\t";
			for(int i = 0; i < eles.size(); i++){
				ele = eles.get(i);
				printNodes(ele, depth);
			}
		}
	}
		
	
}
